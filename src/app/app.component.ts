import {AfterContentChecked, Component, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {SocketClientService} from './service/socket-client.service';
// import * as $ from '../assets/jquery.min.js';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, AfterContentChecked, OnDestroy {
  title = 'chatAppTest';
  currentUser: string;
  receiverId;
  message: string;
  username: string;
  sessionId;
  chatWithUser;
  text;
  listUser: any;

  constructor(private webService: SocketClientService) {
  }

  ngAfterContentChecked(): void {
  }

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.webService.onConnect();
    this.webService.getUser().subscribe((res) => {
      console.log('Yes');
    });
    this.webService.listUser.subscribe(res => {
      this.listUser = res;
    });
    this.webService.getNewMessage().subscribe((res) => {
      // console.log(res);
      this.displayMessage(res.user.username, res.message);
    });
  }

  ngOnDestroy(): void {
    this.webService.onDiconnect();
  }

  addNewUser(): void {
    this.webService.addNewuser(this.username);
  }
  sendMessage(): void {
    this.webService.sendMessage(this.sessionId, this.message, this.username);
  }

  selectUser(username): void {
    $('.outgoing_msg').remove();
    $('.incoming_msg').remove();
    // this.chatWithUser = username.username;
    this.receiverId = username.sessionId;
    console.log(username);
  }

  displayMyMessage(username, message): void {
    let div = '<div class=\'me col s12\'>';
    // tslint:disable-next-line:max-line-length
    div += '   <div class=\'avatar tooltipped\' data-position=\'right\' data-tooltip=\'' + username + '\'>' + username.charAt(0).toLocaleUpperCase() + '</div>';
    div += '   <div class=\'message\'>' + message + '</div>';
    div += '</div>';

    $('#messageArea').append(div);
    $('#messageArea').scrollTop($('#messageArea').prop('scrollHeight'));
  }
  displayMessage(from, message): void {
    let div = '<div class=\'incoming col s12\'>';
    // tslint:disable-next-line:max-line-length
    div +=    '   <div class=\'avatar tooltipped\' data-position=\'left\' data-tooltip=\'' + from + '\'>' + from.charAt(0).toLocaleUpperCase() + '</div>';
    div +=    '   <div class=\'message\'>' + message + '</div>';
    div +=    '</div>';

    $('#messageArea').append(div);
    $('#messageArea').scrollTop($('#messageArea').prop('scrollHeight'));
  }

}
