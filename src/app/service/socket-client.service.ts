import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';
import * as io from '../../assets/socket.io.js';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class SocketClientService {
  socket: any;
  readonly socketUrl = location.protocol + '//' + location.hostname + ':1111/chat';
  private userSource = new Subject();
  listUser = this.userSource.asObservable();
  constructor(private http: HttpClient) {}
  onConnect(): void{
    this.socket = io(this.socketUrl);
    this.socket.on('connect', () => {
      console.log('Connected');
    });
  }
  onDiconnect(): void{
    this.socket.on('disconnect', () => {
      alert('disconnected');
    });
  }
  addNewuser(name): void {
    this.socket.emit('userJoin', {
      sessionId : this.socket.id,
      username: name
    });
  }
  sendMessage(id, text, name): void {
    // tslint:disable-next-line:prefer-const
    this.socket.emit('sendMessage', {
      message: text,
      user: {
        sessionId: id,
        username: name
      }
    });
  }
  getUser(): Observable<any>{
    return new Observable((subscriber => {
      this.socket.on('newUser', (data) => {
        subscriber.next(data);
        console.log(this.socket.connect());
        this.getAllUser().subscribe((res) => {
          this.userSource.next(res);
        });
      });
    }));
  }
  getNewMessage(): Observable<any>{
    return new Observable(subscriber => {
      this.socket.on('newMessage', (data, data2) => {
        subscriber.next(data);
      });
    });
  }

  getAllUser(): Observable<any> {
    return this.http.get('http://localhost:8080/api/users');
  }

}
